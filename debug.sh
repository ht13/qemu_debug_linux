#!/bin/bash

if [ $# -ne 1 ]; then
	echo -e "Usage: $0 LinuxDirectory\n"
	exit
fi

LinuxDir="$1"
VMLinux="$LinuxDir/vmlinux"
GDB_64=0 

if [[ $(file $VMLinux) =~ "x86-64" ]]; then
	echo -e "64 bit linux kernel\n"
	QEMU="qemu-system-x86_64"
	Kernel="$LinuxDir/arch/x86_64/boot/bzImage"
	Rootfs="rootfs_64.img.gz"
	GDB="gdb"
	GDBinit="gdbinit_64"
	GDB_64=1

elif [[ $(file $VMLinux) =~ "Intel 80386" ]]; then
	echo -e "32 bit linux kernel\n"
	QEMU="qemu-system-i386"
	Kernel="$LinuxDir/arch/i386/boot/bzImage"
	Rootfs="rootfs_32.img.gz"
	GDB="gdb"
	GDBinit="gdbinit"

elif [[ $(file $VMLinux) =~ "ARM" ]]; then
	echo -e "ARM linux kernel\n"
	QEMU="qemu-system-arm -M versatilepb"
	Kernel="$LinuxDir/arch/arm/boot/zImage"
	#Rootfs="rootfs_32.img.gz"
	GDB="arm-eabi-gdb"
	GDBinit="gdbinit"
	exit
else
	echo -e "unknown kernel!\n"
	exit
fi

$QEMU -kernel $Kernel -initrd $Rootfs -append "root=/dev/ram rdinit=/sbin/init" -s -S &
sleep 1
gnome-terminal -e "$GDB $VMLinux -q -tui -x $GDBinit" &

if [ $GDB_64 -eq 1 ]; then
	echo -e "\033[32m Please wait 10 seconds......\n\033[0m"
	sleep 10
	kill -9 `pgrep gdb`
	gnome-terminal -e "$GDB $VMLinux -q -tui -x $GDBinit" &
fi
echo -e "\033[32m OK! \033[0m"

